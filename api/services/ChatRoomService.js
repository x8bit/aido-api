var _ = require('lodash');
var Promise = require('bluebird');

module.exports = {

    getChatByMedicId: function (caseId, medicId) {
        var response = null;
        var _medicId = medicId;
        return new Promise(function (resolve, reject) {
            ChatRoom.find({ where: { caseChat: caseId } })
                .populate(['caseChat', 'participants'])
                .then(function (chats) {
                    for (var i = 0; i < chats.length; i++) {
                        var found = _.filter(chats[i].participants, function (p) {
                            return p.id = _medicId
                        });
                        if (found.length > 0) {
                            response = JSON.parse(JSON.stringify(chats[i]));
                            break;
                        }
                    }
                    return resolve(response);
                }).catch(function (error) {
                    return reject(error);
                });
        });
    }

};