module.exports = {
	history: {
		start: function (id, description, name, cb) {
			return CaseHistory.create({
				case: id,
				event: 'chat-started',
				description: description,
				value: name,
			});
		},
		end: function (id, description, name, cb) {
			return CaseHistory.create({
				case: id,
				event: 'chat-ended',
				description: description,
				value: name,
			});
		},
		update: function (id, description, name, cb) {
			return CaseHistory.create({
				case: id,
				event: 'chat-updated',
				description: description,
				value: name,
			});
		},
		addSymptoms: function(id, symptoms) {
			return CaseHistory.create({
				case: id,
				event: 'symptoms-added',
				symptom: symptoms.map(function (s) {
					return s.id;
				})
			});
		},
		addNote: function(id, note) {
			return CaseHistory.create({
				case: id,
				event: 'note-added',
				medicNote: note.id
			});
		},
		addPrescription: function(id, prescription) {
			return CaseHistory.create({
				case: id,
				event: 'prescription-added',
				prescription: prescription.id
			});
		}
	}
};
