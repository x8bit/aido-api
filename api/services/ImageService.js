const path = require('path');
const gm = require('gm');

module.exports = {

  resizeImage: function (s_p, fid, filename, w, h) {
    let name = fid + "_" + "thumb" + path.extname(filename);
    let t_p = path.join(process.cwd(), "..", "attachments", name);

    return new Promise((resolve, reject) => {
      gm(s_p).gravity('Center').resize(w, h, '^').write(t_p, function (err, stdout, stderr) {
        if (err) {
          reject(err);
        } else {
          resolve(t_p);
        }
      });
    });
  },

  isImage: function(url) {
    return /^.*\/(jpg|jpeg|gif|png)$/gi.test(url.toLowerCase());
  }
};
