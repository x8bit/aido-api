var Promise = require('promise');
var EJS = require("ejs");
module.exports = {
	renderFile: function(path, data){
		return new Promise(function (resolve, reject) {
			EJS.renderFile(path, data, function ( err, html ) {
				if (err) {
					reject(err);
				} else{
					resolve(html);
				}
			});
		});
	}
};