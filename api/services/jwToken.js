const jwt = require('jsonwebtoken');
const secret = sails.config.session.secret;

module.exports = {

	issue: function (payload) {
		return jwt.sign(payload, secret, { expiresIn: '72h' });
	},

	verify: function (token, callback) {
		return jwt.verify(token, secret, {}, callback);
	}

};
