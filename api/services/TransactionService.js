var conekta = require('conekta');
conekta.api_key = 'key_brv3xvnzZ5vqQAWzycj6nA';
conekta.locale = 'es';
var Promise = require("bluebird");
var paypal = require('paypal-rest-sdk');
paypal.configure({
  'mode': 'sandbox', //sandbox or live
  'client_id': 'AWN1S6mRaITLyQ8RyyqlqJjrNzMwCKDE2zbEq5yL__tfs5zPLP-z3FXyyivcJf3kThc8sCXjZNGsLajM',
  'client_secret': 'ECIQ2SJm7e7jpkRiDwDsJr_J2Xt0PZizQcI-WUW1d2NuUvo9E0XIJ2givpOwR1Y7ga32oBI4-czeBwXd'
});

module.exports = {

    config: {},

    getCustomerInfo: function(userId) {
        return new Promise(function(resolve, reject){
            User.findOne({id: userId}).then(function(user){
                if (user && user.customerId) {
                    conekta.Customer.find(user.customerId, function(err, customer) {
                        if (err) reject(err);
                        resolve(customer.toObject());
                    });
                } else {
                    resolve({});
                }
            }).catch(function(error) {
                reject(error);
            });
        });
    },

    createPaymentSource: function(customerId, token) {
        var token = token;
        return new Promise(function(resolve, reject){
            conekta.Customer.find(customerId, function(err, customer) {
                if (err) reject(err);
                customer.createPaymentSource({
                    type: "card",
                    token_id: token
                }, function(err, res) {
                    if (err) reject(err);
                    resolve(res);
                });
            });
        });
    },

    updatePaymentSource: function(customerId, cardInfo, index) {
        return new Promise(function(resolve, reject){
            conekta.Customer.find(customerId, function(err, customer) {
                if (err) reject(err);
                customer.payment_sources.get(index).update(cardInfo, function(err, paymentSource) {
                    if (err) reject(err);
                    resolve(paymentSource);
                });
            });
        });
    },

    deletePaymentSource: function(customerId, index) {
        return new Promise(function(resolve, reject){
            conekta.Customer.find(customerId, function(err, customer) {
                if (err) reject(err);
                customer.payment_sources.get(index).delete(function(err, resp) {
                    if (err) reject(err);
                    resolve(resp);
                })
            });
        });
    },

    createCustomer: function(userId, _token) {
        var token = _token;
        console.log(token);
        return new Promise(function(resolve, reject){
            User.findOne({id: userId}).then(function(user){
                conekta.Customer.create({
                    'name': user.name + " " + user.lastname,
                    'email': user.email,
                    'phone': '+5215555555555'
                }, function(err, res) {
                    if (err) {
                        reject(err);
                    }
                    console.log(res, token);
                    TransactionService.createPaymentSource(res.toObject().id, token).then(function(){
                        User.update({id: user.id}, {customerId: res.toObject().id}).then(function(userUpdated){
                            conekta.Customer.find(userUpdated.customerId, function(err, customer) {
                                if (err) {
                                    reject(err);
                                }
                                resolve(customer.toObject());
                            });
                        }).catch(function(err){
                            reject(err);
                        });
                    }).catch(function(err){
                        reject(err);
                    });
                });
            }).catch(function(error) {
                reject(error);
            });
        });
    },

    createOrder: function(customer, payment, item) {
        return new Promise(function(resolve, reject){
            conekta.Order.create({
                "currency": "MXN",
                "customer_info": {
                    "customer_id": customer
                },
                "line_items": [item],
                "charges": [{
                    "payment_method": {
                        "type": "default"
                    }
                }]
            }, function(err, res) {
                if (err) {
                    console.log(err);
                    reject(err);
                }
                resolve(res);
            });
        });
    },

    createTransaction: function(customer, patientId, medicId, paymentSource) {
        return new Promise(function(resolve, reject){
            var aidoFee = 0.0;
            var medicRate = 0.0;
            var order = null;
            var transaction = null;
            var medic = null;
            var payment = null;
            var fee = null;
            var config = null;

            Config.find().then(function(c){
                config = c[0];
                aidoFee = (config.aidoFee) ? (config.aidoFee / 100) : 0.1;
                return User.findOne({id: medicId});
            }).then(function(medicFound){
                if (!medicFound) {
                    reject({error: 'no se encontro el medico'});
                }
                medic = medicFound;
                if (!medic.rate) {
                    medic.rate = config.minRate;
                }
                medicRate = medic.rate * 100;
                var lineItem = {
                    name: 'Consulta - ' + medic.name + " " + medic.lastname,
                    unit_price: medicRate,
                    quantity: 1
                };
                return TransactionService.createOrder(customer, paymentSource, lineItem);
            }).then(function(newOrder){
                order = newOrder;
                var orderObj = order.toObject();
                if (orderObj.payment_status == 'paid') {
                    var transactionObj = {
                        transaction_id: orderObj.id,
                        user: patientId,
                        paymentMethod: 1,
                        medic: medic.id,
                        amount: Math.ceil(orderObj.amount / 100).toFixed(2),
                        iva: 0,
                        total: Math.ceil(orderObj.amount / 100).toFixed(2),
                        comments: 'Ninguno'
                    };
                    return Transaction.create(transactionObj);
                } else {
                    reject({error: 'error en pago', status: orderObj.payment_status});
                }
            }).then(function(newTransaction){
                transaction = newTransaction;
                var paymentObj = {
                    user: medic.id,
                    transaction: transaction.id,
                    amount: transaction.amount - (transaction.amount * aidoFee)
                }
                return Payment.create(paymentObj);
            }).then(function(newPayment){
                payment = newPayment;
                var feeObj = {
                    user: 1,
                    transaction: transaction.id,
                    amount: (transaction.amount * aidoFee).toFixed(2)
                }
                return Fee.create(feeObj);
            }).then(function(newFee){
                fee = newFee;
                var responseObject = {
                    order: order.toObject(),
                    transaction: transaction,
                    medic: medic,
                    payment: payment,
                    fee: fee
                }
                resolve(responseObject);
            }).catch(function(err){
                console.error(err);
                reject(err);
            });
        });
    },

    createTransactionPaypal: function(patientId, medicId, paymentId) {
        return new Promise(function(resolve, reject){
            var aidoFee = 0.1;
            var conektaFee = 0.029;
            var medicRate = 0.0;
            var order = null;
            var transaction = null;
            var medic = null;
            var _payment = null;
            var fee = null;

            Config.find().then(function(c){
                config = c[0];
                aidoFee = (config.aidoFee) ? (config.aidoFee / 100) : 0.1;
                return User.findOne({id: medicId});
            }).then(function(medicFound){
                if (!medicFound) {
                    reject({error: 'no se encontro el medico'});
                }
                medic = medicFound;
                if (!medic.rate) {
                    medic.rate = config.minRate;
                }
                medicRate = (medic.rate).toFixed(2);
                var transactionObj = {
                    transaction_id: paymentId,
                    user: patientId,
                    medic: medic.id,
                    paymentMethod: 2,
                    amount: parseFloat(medicRate).toFixed(2),
                    iva: 0,
                    total: parseFloat(medicRate).toFixed(2),
                    comments: 'Ninguno'
                };
                return Transaction.create(transactionObj);
            }).then(function(newTransaction){
                transaction = newTransaction;
                var paymentObj = {
                    user: medic.id,
                    transaction: transaction.id,
                    amount: transaction.amount - (transaction.amount * aidoFee)
                }
                return Payment.create(paymentObj);
            }).then(function(newPayment){
                _payment = newPayment;
                var feeObj = {
                    user: 1,
                    transaction: transaction.id,
                    amount: (transaction.amount * aidoFee).toFixed(2)
                }
                return Fee.create(feeObj);
            }).then(function(newFee){
                fee = newFee;
                var responseObject = {
                    transaction: transaction,
                    medic: medic,
                    payment: _payment,
                    fee: fee
                }
                resolve(responseObject);
            }).catch(function(err){
                console.error(err);
                reject(err);
            });
        });
    }

};