var Request = require('request');
var Promise = require('bluebird');
var _ = require('lodash');

module.exports = {

    send: function (notificationObject) {
        return new Promise(function (resolve, reject) {
            Request.post({
                headers: {
                    "content-type" : "application/json",
                    "Authorization": "key=AAAAxCo3lqk:APA91bE5zrTD7gMkJw4O16dlnA_e49TaL_3NV0ase146XyEyk71o2ESotOzRJwdjlYq8u8QTuyszHMkrlqp_d2gBlTZCgaOqLd4KSbAHRMSRFpGs37sfVEHRjKy-6-jX0BHVDLN_AQZ3"
                },
                url: 'https://fcm.googleapis.com/fcm/send',
                body: JSON.stringify(notificationObject)
            }, function(error, response, body){
                if (error) {
                    reject(error);
                } else {
                    resolve(response);
                }
            });
        });
    },

    updateBadges: function (_caseId, _chatId, isMedic) {
        var caseCount = 0;
        var chatCount = 0;

        return Case.findOne({ where: { id: _caseId } }).then(function (caso) {
            var badge = (isMedic) ? caso.medicBadge : caso.patientBadge;
            caseCount = parseInt(badge || 0) + 1;
            console.log('caseCount:',caseCount);
            return Case.update({ where: { id: _caseId } }, { badge: caseCount });
        }).then(function (casoUpdated) {
            return ChatRoom.findOne({ where: { id: _chatId } });
        }).then(function (chatRoom){
            var msgCount = (isMedic) ? chatRoom.medicMessageCount : chatRoom.patientMessageCount;
            chatCount = parseInt(msgCount || 0) + 1;
            console.log('chatCount',chatCount);
            return ChatRoom.update({ where: { id: _chatId } }, { messageCount: chatCount });
        });
    },

    sendNotification: function (userId, payload) {
        var _receiver = {};
        return NotificationService.getRecieverInfo(userId)
            .then(function (receiver) {
                _receiver = receiver;
                var silent = (parseInt(_receiver.activeChat) === parseInt(payload.chatId)) ? true : false;
                if (typeof _receiver.token !== 'undefined') {
                    var notification = NotificationService.getNotificationObject(_receiver.token, payload, _receiver.platform, silent);
                    return NotificationService.send(notification);
                } else {
                    return NotificationService.updateBadges(payload.caseId, payload.chatId, NotificationService.isMedic(_receiver));
                }
            })
    },

    sendMultiNotification: function (userId, payload) {
        var _receiver = {};
        return NotificationService.getRecieverInfo(userId)
            .then(function (receiver) {
                _receiver = receiver;
                var silent = (parseInt(_receiver.activeChat) === parseInt(payload.chatId)) ? true : false;
                var promises = [];
                _.forEach(_receiver.devices, function (r) {
                    var notification = NotificationService.getNotificationObject(r.token, payload, r.platform, silent);
                    promises.push(NotificationService.send(notification));
                });
                NotificationService.updateBadges(payload.caseId, payload.chatId, NotificationService.isMedic(_receiver)).then(function (r) {
                    console.log('listo:', r);
                });
                return Promise.all(promises);
            });
    },

    getNotificationObject: function (_to, _payload, _platform, _silent) {
        var _notificationObject = {};

        if (_platform === 'Android') {
            _notificationObject = {
                to: _to
            };
            if (_silent) {
                delete _payload.priority;
                delete _payload['content-available'];
                delete _payload['force-start'];
            }
            _notificationObject.data = _payload;

        } else {
            _notificationObject = {
                content_available: true,
                notification: {
                    title: _payload.title,
                    body: _payload.message
                },
                to: _to
            };
            if (_silent) {
                delete _payload.title;
                delete _payload.message;
                delete _payload.priority;
                delete _payload['content-available'];
                delete _payload['force-start'];
                delete _notificationObject.content_available
                _notificationObject.notification.title = '';
                _notificationObject.notification.body = '';
            }
            _notificationObject.data = _payload;
        }
        return _notificationObject;
    },

    isMedic: function (_receiver) {
        return _receiver.role[0].id === 3;
    },

    getRecieverInfo: function (userId) {
        return new Promise(function (resolve, reject) {
            User.findOne({ where: { id: userId } })
                .populateAll()
                .then(function (user) {
                    resolve(user);
                })
                .catch(function (error) {
                    reject(error);
                });
        });
    }

};