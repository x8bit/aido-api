module.exports = {
  register: function(term, status) {
    return SearchLog.create({
      keyword: term,
      found: status
    }).exec(function(err, record){
      if (err) { return res.serverError(err); }
      return record;
    });
  }
};
