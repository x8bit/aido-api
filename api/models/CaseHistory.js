/**
 * CaseHistory.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    case: {
      model: 'case'
    },
    event: {
      type: 'string',
      enum: ['chat-started', 'chat-updated', 'symptoms-added', 'participant-added', 'chat-ended', 'prescription-added', 'note-added', 'message-added']
    },
    description: {
      type: 'text'
    },
    value: {
      type: 'string'
    },
    symptom: {
      collection: 'symptom'
    },
    participant: {
      model: 'user'
    },
    medicNote: {
      model: 'note'
    },
    prescription: {
      model: 'prescription'
    }
  }
};

