/**
 * Transaction.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    transaction_id: {
      type: 'string'
    },
    consultation: {
      model: 'chatRoom'
    },
    user: {
      model: 'user'
    },
    medic: {
      model: 'user'
    },
    paymentMethod: {
      model: 'paymentMethod'
    },
    amount: {
      type: 'float'
    },
    iva: {
      type: 'float'
    },
    total: {
      type: 'float'
    },
    comments: {
      type: 'text'
    }
  }
};

