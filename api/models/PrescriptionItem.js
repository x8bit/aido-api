/**
 * PrescriptionItem
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    item: {
      model: 'prescription'
    },
    medicines: {
      model: 'medicine'
    },
    dose: {
      type: 'text'
    },
    indication: {
      type:'text'
    }
  }
};
