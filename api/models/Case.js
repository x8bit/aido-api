/**
 * Case.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    defaultAvatar: {
      type: 'string'
    },
    badge: {
      type: 'integer'
    },
    patientBadge: {
      type: 'integer'
    },
    medicBadge: {
      type: 'integer'
    },
    patient: {
      model: 'user'
    },
    name: {
      type: 'string',
      required: true
    },
    description: {
      type: 'text',
      required: true
    },
    attachments: {
      collection: 'attachment',
      via: 'case'
    },
    symptoms: {
      collection: 'symptom',
      via: 'caseSymptom'
    },
    chats: {
      collection: 'chatRoom',
      via: 'caseChat'
    },
    status: {
      type: 'string',
      required: true
    },
    currentMedicines: {
      type: 'text'
    },
    recommendation: {
      type: 'text'
    },
    history: {
      collection: 'caseHistory',
      via: 'case'
    },
    evolutionTime: {
      type: 'string'
    },
    symptomsDate: {
      type: 'date'
    },
    firebaseAttachments: {
      type: 'array'
    }
  }
};

