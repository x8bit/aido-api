/**
 * Attachment.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    filename: {
      type: 'string',
      required: true
    },
    size: {
      type: 'integer'
    },
    type: {
      type: 'string'
    },
    ext: {
      type: 'string'
    },
    thumbnail: {
      type: 'string'
    },
    path: {
      type: 'string'
    },
    case: {
      model: 'case'
    },
    message: {
      model: 'message'
    }
  }
};

