/**
 * ChatRoom.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    caseChat: {
      model: 'case'
    },
    messages: {
      collection: 'message',
      via: 'chatMessage'
    },
    notes: {
      collection: 'note',
      via: 'chatNote'
    },
    participants: {
      collection: 'user'
    },
    lastMessage: {
      type: 'string'
    },
    patientMessageCount: {
      type: 'integer'
    },
    medicMessageCount: {
      type: 'integer'
    },
    treatment: {
      type: 'text'
    },
    recommendation: {
      type: 'text'
    },
    prescription: {
      collection: 'prescription',
      via: 'chatPrescription'
    },
    status: {
      type: 'string',
      defaultsTo: 'open'
    }
  }
};

