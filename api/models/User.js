/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
const bcrypt = require('bcryptjs');
const crypto = require('crypto');

module.exports = {

  attributes: {
    email: {
      type: 'string',
      unique: true,
      required: true,
      email: true
    },
    name: {
      type: 'string',
      required: true
    },
    lastname: {
      type: 'string',
      required: true
    },
    hashedPassword: {
      type: 'string'
    },
    token: {
      type: 'string'
    },
    role: {
      collection: 'role',
      required: true
    },
    specialties: {
      collection: 'specialty',
      via: 'doctors',
      dominant: true
    },
    profile: {
      model: 'profile'
    },
    active: {
      type: 'boolean'
    },
    onlineStatus: {
      type: 'boolean',
      defaultsTo: true
    },
    cases: {
      collection: 'case',
      via: 'patient'
    },
    recoveryToken: {
      type: 'string'
    },
    customerId: {
      type: 'string'
    },
    rate: {
      type: 'float'
    },
    platform: {
      type: 'string',
      defaultsTo: 'Android'
    },
    activeChat: {
      type: 'integer',
    },
    devices: {
      collection: 'device'
    },
    toJSON: function() {
      var obj = this.toObject();
      delete obj.hashedPassword;
      return obj;
    }
  },

  beforeCreate: function (attrs, next) {
    bcrypt.genSalt(8, function (err, salt) {
      if (err) { next(err); }
      bcrypt.hash(attrs.password, salt, function(err, hash) {
        if (err) { next(err); }

        delete attrs.password
        attrs.hashedPassword = hash;
        if (attrs.role.id === 3 || attrs.role === 3) {
          attrs.active = false;
        } else {
          attrs.active = true;
        }
        next();
      });
    });
  },

  beforeUpdate: function (attrs, next) {
    if (attrs.password) {
      bcrypt.genSalt(8, function(err, salt) {
        if (err) { next(err); }
        bcrypt.hash(attrs.password, salt, function(err, hash) {
          if (err) { next(err); }

          delete attrs.password
          attrs.hashedPassword = hash;
          next();
        });
      });
    } else {
      next();
    }
  },

  comparePassword: function (password, hashedPassword, next) {
    bcrypt.compare(password, hashedPassword, function (err, match) {
      if (err) { next(err); }

      if (match) {
        next(null, true);
      } else {
        next(err);
      }
    });

  }


};

