/**
 * Profile.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    defaultAvatar: {
      type: 'string'
    },
    birthday: {
      type: 'date'
    },
    sex: {
      type: 'string'
    },
    height: {
      type: 'float'
    },
    weight: {
      type: 'float'
    },
    bodyMassIndex: {
      type: 'float'
    },
    phoneNumber: {
      type: 'string'
    },
    bloodType: {
      type: 'string'
    },
    allergies: {
      type: 'text'
    },
    previousSurgeries: {
      type: 'text'
    },
    knownCurrentConditions: {
      type: 'text'
    },
    smokes: {
      type: 'string'
    },
    drinks: {
      type: 'string'
    },
    excersiseFrequence: {
      type: 'string'
    },
    medicines: {
      type: 'text'
    },
    bio: {
      type: 'text'
    },
    professionalCertificate: {
      type: 'string'
    },
    specialistCertificate: {
      type: 'string'
    },
    councilCertification: {
      type: 'string'
    },
    location: {
      type: 'string'
    },
    state: {
      type: 'string'
    },
    city: {
      type: 'string'
    },
    rate: {
      type: 'float'
    }
  },

  beforeCreate: function (attrs, next) {

    if (attrs.height && attrs.height > 0 && attrs.weight && attrs.weight > 0) {
      attrs.bodyMassIndex = (attrs.weight / (attrs.height * attrs.height)).toFixed(2);
    }
    next();

  },

  beforeUpdate: function (attrs, next) {

    if (attrs.height && attrs.height > 0 && attrs.weight && attrs.weight > 0) {
      attrs.bodyMassIndex = (attrs.weight / (attrs.height * attrs.height)).toFixed(2);
    }
    next();

  }
};
