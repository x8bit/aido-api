/**
 * SpecialtyController
 *
 * @description :: Server-side logic for managing specialties
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var Q = require('q');
var _ = require('lodash');

module.exports = {

	findMedics: function(req, res) {
		const search = req.query.search;
    let query = {};
    if (search) {
      query = { where: { name: { contains: search.toLowerCase() } }, sort: { name: 1 } };
    } else {
      query = { sort: { name: 1 } };
    }
    Specialty.find(query).populate(['doctors']).then(function(specialties) {
      var specialtiesWithDoctors = specialties.filter((s) => { return s.doctors.length > 0; })
      if (search && specialtiesWithDoctors.length !== 0) SearchLogService.register(search, (specialtiesWithDoctors.length !== 0));

      var promises = [];
      _.forEach(specialtiesWithDoctors, function(s) {

        var defer = Q.defer();
        promises.push(defer.promise);
        User.find({ where: { id: s.doctors.map(function(d) { return d.id; }), active: true }, sort: { name: 1 } }).populate(['profile']).exec(function(err, doctors) {
          if (err) { return res.serverError(err); }
          defer.resolve(doctors);
        });

      });

      Q.all(promises).then(function(values) {
        var i = 0;

        Config.find().exec(function(err, c) {
          if (err) { return res.serverError(err); }
          var fullSpecialtiesWithDoctors = _.map(specialtiesWithDoctors, function(s) {

            s.doctors = values[i].map(function(m){
              if (typeof m.rate == 'undefinded' || !m.rate) {
                console.log(m);
                m.rate = c[0].minRate || 0;
              }
              return m;
            });
            i++;
            return s;
          });

          return res.ok(fullSpecialtiesWithDoctors);
        });

      }).catch(function() {
        return res.serverError(err);
      });


    }).catch(function(err) {
      return res.serverError(err);
    });
	}


};

