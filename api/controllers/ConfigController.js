/**
 * ConfigController
 *
 * @description :: Server-side logic for managing configs
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    list: function (req, res) {
        Config.find().then(function (config) {
            var _c = {}
            if (config.length) {
                _c = config[0];
            }
            res.view('config', {
                config: _c,
                alert: ''
            });

        }).catch(function (error) {
            return res.serverError(err);
        });
    },

    actualiza: function (req, res) {
        console.log(req.body);
        Config.find().then(function(resp){
            console.log(resp);
            if(resp.length === 0) {
                Config.create(req.body).then(function(c){
                    res.view('config', {
                        config: c[0],
                        alert: 'Se guardaron los cambios exitosamente'
                    });
                }).catch(function(error){
                    return res.serverError(error);
                });
            } else {
                Config.update({id: resp[0].id}, req.body).then(function(c){
                    res.view('config', {
                        config: c[0],
                        alert: 'Se guardaron los cambios exitosamente'
                    });
                }).catch(function(error){
                    return res.serverError(error);
                });
            }
        }).catch(function(error){
            return res.serverError(error);
        });
    }
};

