var Request = require('request');

module.exports = {
    // Objeto de notificacion de mensajes notifData

    // tipo: 'mensaje',
    // caseId: $scope.chatInfo.caseChat.id,
    // chatId: chat,
    // priority: 2,
    // 'notId': '' + chat,
    // style: 'inbox'
    // notifData['content-available'] = 1;
    // notifData['force-start'] = 1;

    send: function (req, res) {
        var to = req.body.to;
        var title = req.body.title;
        var msg = req.body.message;
        var payload = req.body.payload;
        var multiple = req.body.multiple || false;

        payload.title = title;
        payload.message = msg;

        if (multiple === true) {
            NotificationService.sendMultiNotification(to, payload)
                .then(function (responses) {
                    console.log(responses);
                    return res.ok(responses);
                })
                .catch(function (error) {
                    return res.serverError(error);
                });
        } else {
            NotificationService.sendNotification(to, payload)
                .then(function (response) {
                    console.log(response.body);
                    return res.ok(response.body);
                })
                .catch(function (error) {
                    return res.serverError(error);
                });
        }
    }

    // send: function(req, res) {
    //     var to = req.body.to;
    //     var title = req.body.title;
    //     var msg = req.body.message;
    //     var payload = req.body.payload;
    //     var multiple = req.body.multiple || false;
    //     var os = req.body.os;
    //     payload.title = title;
    //     payload.message = msg;

    //     User.findOne({ id: to }).populate('role').then(function(user) {
    //         var notificationObject = {};
    //         var isMedic = (user.role[0].id === 3) ? true : false;

    //         if (user.platform === 'Android') {
    //             if (parseInt(user.activeChat) === parseInt(payload.chatId)) {
    //                 console.log('entro en android');
    //                 delete payload.priority;
    //                 delete payload['content-available'];
    //                 delete payload['force-start'];
    //                 notificationObject = {
    //                     to: user.token,
    //                     data: payload
    //                 };
    //             } else {
    //                 notificationObject = {
    //                     to: user.token,
    //                     data: payload
    //                 };
    //             }
    //         } else {
    //             if (parseInt(user.activeChat) === parseInt(payload.chatId)) {
    //                 console.log('entro en ios');
    //                 delete payload.title;
    //                 delete payload.message;
    //                 delete payload.priority;
    //                 delete payload['content-available'];
    //                 delete payload['force-start'];
    //                 notificationObject = {
    //                     notification: {
    //                         title: '',
    //                         body: ''
    //                     },
    //                     to: user.token,
    //                     data: payload
    //                 };
    //             } else {
    //                 delete payload['content-available'];
    //                 delete payload['force-start'];
    //                 notificationObject = {
    //                     content_available: true,
    //                     notification: {
    //                         title: title,
    //                         body: msg
    //                     },
    //                     to: user.token,
    //                     data: payload
    //                 };
    //             }
    //         }

    //         if (user.token) {
    //             console.log('con token');
    //             if (payload.tipo === 'mensaje') {
    //                 var chatId = payload.chatId;
    //                 var caseId = payload.caseId;
    //                 var caseObject = (isMedic) ? { medicBadge: 0 } : { patientBadge: 0 };
    //                 var chatObject = (isMedic) ? { medicMessageCount: 0 } : { patientMessageCount: 0 };
    //                 Case.update({ where: { id: parseInt(caseId) } }, caseObject).then(function(updatedCase) {
    //                     console.log(updatedCase);
    //                     return ChatRoom.update({ where: { id: parseInt(chatId) } }, chatObject);
    //                 }).then(function(updatedChat) {
    //                     console.log(updatedChat);
    //                     console.log("Listo");
    //                 }).catch(function(error) {
    //                     console.error(error);
    //                 });
    //             }
    //             console.log(JSON.stringify(notificationObject));
    //             Request.post({
    //                 headers: {
    //                     "content-type" : "application/json",
    //                     "Authorization": "key=AAAAxCo3lqk:APA91bE5zrTD7gMkJw4O16dlnA_e49TaL_3NV0ase146XyEyk71o2ESotOzRJwdjlYq8u8QTuyszHMkrlqp_d2gBlTZCgaOqLd4KSbAHRMSRFpGs37sfVEHRjKy-6-jX0BHVDLN_AQZ3"
    //                 },
    //                 url: 'https://fcm.googleapis.com/fcm/send',
    //                 body: JSON.stringify(notificationObject)
    //             }, function(error, response, body){
    //                 console.log('e ->', error);
    //                 return res.ok(response);
    //             });
    //         } else {
    //             console.log('sin token');
    //             if (payload.tipo === 'mensaje') {
    //                 var chatId = payload.chatId;
    //                 var caseId = payload.caseId;
    //                 var caseCount = 0;
    //                 var chatCount = 0;
    //                 Case.findOne({ where: { id: parseInt(caseId) } }).then(function (caso) {
    //                     var badge = (isMedic) ? caso.medicBadge : caso.patientBadge;
    //                     caseCount = parseInt(badge || 0) + 1;
    //                     console.log(caseCount);
    //                     return Case.update({ where: { id: parseInt(caseId) } }, { badge: caseCount });
    //                 }).then(function (casoUpdated) {
    //                     console.log(chatId);
    //                     return ChatRoom.findOne({ where: { id: parseInt(chatId) } });
    //                 }).then(function (chatRoom){
    //                     var msgCount = (isMedic) ? chatRoom.medicMessageCount : chatRoom.patientMessageCount;
    //                     console.log(chatRoom.messageCount, chatId);
    //                     chatCount = parseInt(msgCount || 0) + 1;
    //                     console.log(chatCount);
    //                     return ChatRoom.update({ where: { id: parseInt(chatId) } }, { messageCount: chatCount });
    //                 }).then(function(chatUpdated) {
    //                     console.log("Listo", chatUpdated);
    //                 }).catch(function(error) {
    //                     console.error(error);
    //                 });
    //             }
    //             return res.ok({ message: 'no token' });
    //         }
    //     }).catch(function(error) {
    //         return res.serverError(error);
    //     });
    // }

};