/**
 * NoteController
 *
 * @description :: Server-side logic for managing notes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var Request = require('request');
module.exports = {

	create: function(req, res) {
    var data_from_client = req.params.all();

    Note.create(data_from_client).exec(function(error, data_from_server) {
      if (error) { return res.serverError(error); }

      var newNote = data_from_server.toObject();

      ChatRoom.findOne({ id: newNote.chatNote }).populate(['caseChat', 'participants']).exec(function (err, chat) {
        if (err) { return res.serverError(err); }

        let receiver = chat.participants.filter(function(p) {
            return p.id !== newNote.sender;
        });
        let notification = {
            tokens: [receiver[0].token],
            profile: 'aido_android',
            notification: {
                title: 'Nota: ' + chat.caseChat.name,
                message: newNote.text,
                payload: {
                    type: 'note',
                    case: chat.caseChat,
                    chatNote: data_from_server.chatNote
                }
            }
        };

        Request.post({
            headers: {
                "content-type" : "application/json",
                "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIxZTE1ODM3Ny02Y2VkLTRkMWQtYmZlOC03YmRkODZiYmI0ZjUifQ.z5-ph6RLXo1APGR37KShrC61zrivihuuJ7V9RMwtzUw"
            },
            url: 'https://api.ionic.io/push/notifications',
            body: JSON.stringify(notification)
        }, function(error, response, body){
            console.log('e ->', error);
        });

        CaseService.history.addNote(chat.caseChat.id, newNote).then(function(){
          return res.ok(data_from_server);
        });
      });

    });
  }
};

