/**
 * FeeController
 *
 * @description :: Server-side logic for managing Fees
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	list: function(req, res) {
        Fee.find().populateAll().then(function(_fees){
            res.view('fees', {
                fees: _fees
            });
        }).catch(function(err) {
            return res.serverError(err);
        });
    },
};

