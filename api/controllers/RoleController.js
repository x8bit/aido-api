/**
 * RoleController
 *
 * @description :: Server-side logic for managing roles
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	getAppRoles: function (req, res) {
		Role.find({ type: { '!': 'admin' } }).then(function (roles) {
			return res.ok(roles);
		}).catch(function (error) {
			return res.serverError(err);
		});
	}

};

