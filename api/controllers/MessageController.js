/**
 * MessageController
 *
 * @description :: Server-side logic for managing messages
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var Request = require('request');
module.exports = {

	create: function(req, res) {

        var data_from_client = req.params.all();

        Message.create(data_from_client).exec(function(error, data_from_server) {
            if (error) { return res.serverError(error); }

            ChatRoom.findOne({ id: data_from_server.chatMessage }).populate(['caseChat', 'participants']).exec(function (err, chat) {
                if (err) { return res.serverError(err); }
                console.log(data_from_server.sender);
                let receiver = chat.participants.filter(function(p) { return p.id !== data_from_server.sender });
                let notification = {
                    tokens: [receiver[0].token],
                    profile: 'aido_android',
                    notification: {
                        title: chat.caseChat.name,
                        message: data_from_server.messageText,
                        payload: {
                            type: 'message',
                            case: chat.caseChat,
                            caseChat: data_from_server.chatMessage
                        }
                    }
                };

                console.log(receiver);

                Request.post({
                    headers: {
                        "content-type" : "application/json",
                        "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIxZTE1ODM3Ny02Y2VkLTRkMWQtYmZlOC03YmRkODZiYmI0ZjUifQ.z5-ph6RLXo1APGR37KShrC61zrivihuuJ7V9RMwtzUw"
                    },
                    url: 'https://api.ionic.io/push/notifications',
                    body: JSON.stringify(notification)
                }, function(error, response, body){
                    console.log('e ->', error);
                });
            });

            return res.ok(data_from_server);
        });
    }
};
