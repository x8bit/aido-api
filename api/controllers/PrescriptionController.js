/**
 * PrescriptionController
 *
 * @description :: Server-side logic for managing Prescription
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var Request = require('request');
var _ = require('lodash');
var Q = require('q');

module.exports = {

	create: function(req, res) {
    var data_from_client = req.params.all();
    var prescriptor = req.query.user;

    Prescription.create(data_from_client).exec(function(error, data_from_server) {
      if (error) { return res.serverError(error); }

      var newPrescription = data_from_server.toObject();

      ChatRoom.findOne({ id: newPrescription.chatPrescription }).populate(['caseChat', 'participants']).exec(function (err, chat) {
        if (err) { return res.serverError(err); }

        let receiver = chat.participants.filter(function(p) {
        	return p.id !== prescriptor;
        });
        let notification = {
            tokens: [receiver[0].token],
            profile: 'aido_android',
            notification: {
                title: chat.caseChat.name,
                message: 'Receta agregada',
                payload: {
                		type: 'prescription',
                    case: chat.caseChat,
                    prescription: newPrescription.chatPrescription
                }
            }
        };

        Request.post({
            headers: {
                "content-type" : "application/json",
                "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIxZTE1ODM3Ny02Y2VkLTRkMWQtYmZlOC03YmRkODZiYmI0ZjUifQ.z5-ph6RLXo1APGR37KShrC61zrivihuuJ7V9RMwtzUw"
            },
            url: 'https://api.ionic.io/push/notifications',
            body: JSON.stringify(notification)
        }, function(error, response, body){
            console.log('e ->', error);
        });

        CaseService.history.addPrescription(chat.caseChat.id, newPrescription).then(function() {
        	return res.ok(data_from_server);
        }).catch(function(err){
        	return res.serverError(err);
        });
      });

    });
  },

	findOne: function (req, res) {
		const prescriptionId = req.param('id');

		if (prescriptionId) {
			Prescription.findOne({ where: { id: prescriptionId } }).populate(['items']).exec(function (err, prescription) {
				if (err) { return res.serverError(err); }
				var fullPrescription = prescription.toObject();

				PrescriptionItem.find({ where: { item: prescriptionId } }).populate(['medicines']).exec(function (err, items) {
					if (err) { return res.serverError(err); }

					fullPrescription.items = []
					fullPrescription.items = items;

					return res.ok(fullPrescription);

				});
			})

		} else {
			return res.serverError({ message: 'prescription not found' });
		}
	},


    findByMedic: function (req, res) {
        const medicId = req.body.id;
        const chatId = req.body.chat;

        console.log(medicId, chatId);
        
        if (medicId && chatId) {
            User.findOne({ where: { id: medicId } }).populate(['profile']).exec(function(err, user) {

                var fullResponse = {};
                fullResponse.medic = user.toObject();
                fullResponse.prescriptions = [];

                Prescription.find({ where: { medic: medicId, chatPrescription: chatId } }).populate(['items']).exec(function (err, prescriptions) {
                    if (err) { return res.serverError(err); }

                    var promises = [];
                    _.forEach(prescriptions, function(p) {
                        var defer = Q.defer();
                        promises.push(defer.promise);

                        PrescriptionItem.find({
                            where: {
                                item: p.id
                            }
                        }).populate(['medicines']).exec(function(err, items) {
                            if (err) {                                
                                defer.reject(err);
                            }
                            p.items = []
                            p.items = items;

                            defer.resolve(p);
                        });

                    });

                    Q.all(promises).then(function(values) {
                        fullResponse.prescriptions = values;
                        return res.ok(fullResponse);
                    });

                });

            });

        } else {
            return res.serverError({ message: 'prescriptions not found' });
        }
    }

};
