/**
 * CaseHistoryController
 *
 * @description :: Server-side logic for managing casehistories
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var _ = require('lodash');
var Q = require('q');

module.exports = {

	findByCase: function(req, res) {
		var caseId = req.query.caseId;

		CaseHistory.find({
			where: {
				case: caseId
			},
			sort: 'createdAt DESC'
		}).populate(['case', 'symptom', 'participant', 'prescription', 'medicNote']).exec(function(err, history) {
			if (err) {
				return res.serverError(err);
			}
			var fullResponse = [];
			var promises = [];

			var prescriptionsEvents = _.filter(history, function(e) {
				return e.event === 'prescription-added' || e.event === 'note-added';
			});

			_.forEach(prescriptionsEvents, function(p) {

				var defer = Q.defer();
				promises.push(defer.promise);

				if (p.event === 'prescription-added') {

					Prescription.findOne({
						where: {
							id: p.prescription.id
						}
					}).populate(['items', 'medic']).exec(function(err, prescription) {
						if (err) {
							return res.serverError(err);
						}

						var fullPrescription = prescription.toObject();

						PrescriptionItem.find({
							where: {
								item: prescription.id
							}
						}).populate(['medicines']).exec(function(err, items) {
							if (err) {
								return res.serverError(err);
							}

							fullPrescription.items = []
							fullPrescription.items = items;
							delete p.prescription;
							p.prescription = fullPrescription;

							defer.resolve(p);
						});

					});

				} else {

					Note.findOne({
						where: {
							id: p.medicNote.id
						}
					}).populate(['sender']).exec(function(err, note) {
						if (err) {
							return res.serverError(err);
						}

						var fullNote = note.toObject();
						delete p.medicNote;
						p.medicNote = fullNote;
						defer.resolve(p);

					});

				}

			});

			Q.all(promises).then(function(values) {

				var nonPrescriptionsEvents = _.filter(history, function(e) {
					return e.event !== 'prescription-added' && e.event !== 'note-added';
				});

				var fullArrays = nonPrescriptionsEvents.concat(values);

				return res.ok(_.orderBy(fullArrays, ['id'], ['desc']));
			}).catch(function() {
				return res.serverError("no promises");
			});


		});
	}

};
