/**
 * CaseController
 *
 * @description :: Server-side logic for managing cases
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var Q = require('q');
var _ = require('lodash');

module.exports = {

	find: function (req, res) {
		let status = req.query.status || 'active';
		let userid = req.query.patient;

		if (userid) {
			Case.find({ where: { patient: userid, status: status }, sort: 'updatedAt DESC' }).populate('patient').exec(function (err, cases) {
				if (err) { return res.serverError(err); }

				return res.ok(cases);
			});
		} else {
			return res.serverError({ message: 'user not found' });
		}

	},

	findMedicCases: function (req, res) {
		const userid = req.query.user;
		const status = req.query.status || 'active';
		const chatStatus = (status === 'active') ? 'open' : 'closed';

		ChatRoom.find({ where:  {status: chatStatus }, sort: 'updatedAt DESC' }).populate(['participants']).exec(function(err, chats) {
			if (err) { return res.serverError(err); }
			var casesArr = [];
			var chatsArr = [];
			for (var i = chats.length - 1; i >= 0; i--) {
				chats[i].participants.forEach(function(p) {
					if (p.id == userid) {
						casesArr.push(chats[i].caseChat);
						chatsArr.push(chats[i].id);
					}
				});
			}
			Case.find({ where: { id: casesArr } , sort: 'updatedAt DESC' }).populate('patient').exec(function (err, cases) {
				if (err) { return res.serverError(err); }
				let fullCase = cases.map(function(c) {
					let idx = casesArr.indexOf(c.id);
					c.chatId = chatsArr[idx];
					return c;
				})
				return res.ok(fullCase);
			});
		});
	},

	findOne: function (req, res) {
		const caseId = req.param('id');

		if (caseId) {
			Case.findOne({ where: { id: caseId } }).populate(['chats', 'symptoms']).exec(function (err, caseFound) {
				if (err) { return res.serverError(err); }
				var fullCase = caseFound.toObject();

				ChatRoom.find({ where: { caseChat: caseId } }).populate(['participants', 'prescription']).exec(function (err, chats) {
					if (err) { return res.serverError(err); }

					fullCase.chats = [];

					var promises = [];
					if (chats.length > 0) {

						_.forEach(chats, function(c) {
							var defer = Q.defer();
							promises.push(defer.promise);

							User.find({ where: { id: c.participants.map(function (p) { return p.id }) } }).populate(['role', 'profile']).exec(function (err, users) {
								if (err) { return res.serverError(err); }
								const medics = _.find(users, (u) => { return u.role[0].id == 3 });
								defer.resolve(medics);
							});

						});

					}

					Q.all(promises).then(function(values) {
						for (var i = chats.length - 1; i >= 0; i--) {
							chats[i].participants = values[i];
						}
						fullCase.chats = chats;
						return res.ok(fullCase);
					});

				});
			})

		} else {
			return res.serverError({ message: 'case not found' });
		}
	},

	create: function (req, res) {

		Case.create(req.body).exec(function (err, caseFound) {
			if (err) { return res.serverError(err); }

			var newCase = caseFound.toObject();

			CaseService.history.start(newCase.id, newCase.description, newCase.name).then(function (history) {

				Symptom.find({ where: { caseSymptom: newCase.id } }).exec(function (err, symptoms) {
					if (err) { return res.serverError(err); }
					if (symptoms.length > 0) {

						CaseService.history.addSymptoms(newCase.id, symptoms).then(function () {
							return res.ok(newCase);
						}).catch(function (err){
							return res.serverError(err);
						});

					} else {
						return res.ok(newCase);
					}
				});

			}).catch(function (err) {
				return res.serverError(err);
			});

		});

	},

	closeCase: function (req, res) {
		var caseId = req.param('id');
		Case.findOne({ where: { id: caseId} }).populateAll().then(function (foundCase) {
			var chatsDeleted = [];
			_.forEach(foundCase.chats, function (chat) {
				chatsDeleted.push( ChatRoom.update({ id: chat.id}, { status: 'closed' }) );
			});
			return Q.all(chatsDeleted);
		}).then(function (chats) {
			return Case.update({ id: caseId }, { status: 'closed' })
		}).then(function (caseDeleted) {
			return CaseService.history.end(caseId.id, 'Caso Cerrado', 'Caso Cerrado')
		}).then(function (history) {
			return res.ok({ msg: 'caso cerrado' });
		}).catch(function (error) {
			return res.serverError(error);
		});
	},

	update: function (req, res) {
		console.log(req.body);
		const history = req.query.history || 'yes';
		Case.findOne({ where: { id: req.param('id') } }).populate(['symptoms']).exec(function (err, caseFound) {
			var oldSymptoms = caseFound.toObject().symptoms;

			console.log(caseFound);

			Case.update({ id: req.param('id') }, req.body).exec(function (err, updatedCase) {
				if (err) { return res.serverError(err); }
				var newCase = updatedCase[0].toObject();

				if (history === 'yes') {

					if (newCase.status === 'closed') {
						CaseService.history.end(newCase.id, 'Caso Cerrado', 'Caso Cerrado').then(function (h) {
							return res.ok(newCase);
						}).catch(function (err) {
							return res.serverError(err);
						});
					} else {
						CaseService.history.update(newCase.id, JSON.stringify(newCase), newCase.name).then(function (history) {

							Symptom.find({ where: { caseSymptom: newCase.id } }).exec(function (err, symptoms) {
								if (err) { return res.serverError(err); }
								if (symptoms.length > 0) {

									var symptomsEdited = false;
									if (oldSymptoms && oldSymptoms.length !== 0) {
										var reqIds = oldSymptoms.map(function (s) { return s.id; });
										var symptomsIds = symptoms.map(function (s) { return s.id; });
										symptomsEdited = !_.isEqual(reqIds, symptomsIds);
									}

									if (symptomsEdited) {
										CaseService.history.addSymptoms(newCase.id, symptoms).then(function () {
											return res.ok(newCase);
										}).catch(function (err){
											return res.serverError(err);
										});
									} else {
										return res.ok(newCase);
									}

								} else {
									return res.ok(newCase);
								}
							});

						}).catch(function (err) {
							return res.serverError(err);
						});
					}

				} else {
					return res.ok(newCase);
				}

			});

		});

	}

};

