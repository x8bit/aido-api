/**
 * PaymentController
 *
 * @description :: Server-side logic for managing Payments
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	list: function(req, res) {
        Payment.find().populateAll().then(function(_payments){
            res.view('payments', {
                payments: _payments
            });
        }).catch(function(err) {
            return res.serverError(err);
        });
    },
};

