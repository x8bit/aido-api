/**
 * ChatRoomController
 *
 * @description :: Server-side logic for managing chatrooms
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var Request = require('request');
var _ = require('lodash');
var Promise = require('bluebird');

module.exports = {

	create: function(req, res) {
      var data_from_client = req.params.all();

      ChatRoomService.getChatByMedicId(data_from_client.caseChat, data_from_client.medicId)
        .then(function (chatFound) {
            if (chatFound) {
                return ChatRoom.update({ id: chatFound.id}, { status: 'open' })
            } else {
                return ChatRoom.create(data_from_client);
            }
        })
        .then(function (data_from_server) {
            return ChatRoom.findOne({ id: data_from_server.id }).populate(['caseChat', 'participants'])
        })
        .then(function (chatResponse) {
            return res.ok(chatResponse);
        })
        .catch(function (error) {
            console.log(error);
        });
  },

	findOne: function (req, res) {
		const chatId = req.param('id');

		if (chatId) {
			ChatRoom.findOne({ where: { id: chatId } }).populate(['notes', 'participants', 'caseChat', 'prescription']).exec(function (err, chat) {
				if (err) { return res.serverError(err); }
				return res.ok(chat);
			});

		} else {
			return res.serverError({ message: 'chat not found' });
		}
	}

};

