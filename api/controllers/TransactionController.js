/**
 * TransactionController
 *
 * @description :: Server-side logic for managing Transactions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var converter = require('json-2-csv');
var Q = require('q');
var _ = require('lodash');
var moment = require('moment');

module.exports = {

    export: function(req, res) {
        var transactionsList = [];
        var paymentsList = [];
        var feesList = [];
        var paymentsPromises = [];
        var feesPromises = [];
        var config = {};

        Config.find().then(function (configs) {
            config = configs[0];
            return Transaction.find().populateAll();
        }).then(function (transactions) {
            transactionsList = transactions;
            _.forEach(transactionsList, function (transaction) {
                paymentsPromises.push(Payment.findOne({ where: { transaction: transaction.id } }));
            });
            return Q.all(paymentsPromises);
        }).then(function (payments) {
            paymentsList = payments;
            _.forEach(transactionsList, function (transaction) {
                feesPromises.push(Fee.findOne({ where: { transaction: transaction.id } }));
            });
            return Q.all(feesPromises);
        }).then(function (fees) {
            feesList = fees;
            transactionsList = _.map(transactionsList, function (transaction) {
                console.log(transaction);
                var p = _.find(paymentsList, function (o) { return o.transaction === transaction.id });
                var f = _.find(feesList, function (o) { return o.transaction === transaction.id });
                transaction.payment = p;
                transaction.fee = f;
                transaction.comisionAido = config.aidoFee;
                transaction.tarifaMininaAido = config.minRate;
                if (typeof transaction.consultation !== 'undefined' && transaction.consultation.status !== 'open') {
                    transaction.chatClosed = transaction.updatedAt;
                    var diff = moment(transaction.createdAt, 'DD/MM/YYYY HH:mm:ss').diff(moment(transaction.updatedAt, 'DD/MM/YYYY HH:mm:ss'));
                    var duration = moment.duration(diff);
                    transaction.chatDuration = Math.floor(duration.asHours()) + moment.utc(diff).format(':mm:ss');
                } else {
                    transaction.chatClosed = '';
                    transaction.chatDuration = 0;
                }
                return transaction;
            });
            converter.json2csv(transactionsList, function (err, csv) {
                var filename = 'transacciones' + new Date().getTime().toString() + '.csv';
                res.attachment(filename);
                res.end(csv, 'UTF-8');
            }, {
                keys: [
                    'id',
                    'createdAt',
                    'user.email',
                    'user.name',
                    'user.lastname',
                    'medic.email',
                    'medic.name',
                    'medic.lastname',
                    'paymentMethod.name',
                    'transaction_id',
                    'amount',
                    'fee.amount',
                    'payment.amount',
                    'comisionAido',
                    'tarifaMininaAido',
                    'chatClosed',
                    'chatDuration'
                ]
            });
        }).catch(function (error) {
            res.serverError(error);
        });
    },

    list: function(req, res) {
        Transaction.find().populateAll().then(function(_transactions){
            res.view('transactions', {
                transactions: _transactions
            });
        }).catch(function(err) {
            return res.serverError(err);
        });
    },

    getCustomer: function(req, res){
        var userId = req.body.userId;
        if (userId) {
            TransactionService.getCustomerInfo(userId).then(function (customer) {
                return res.ok(customer);
            }).catch(function(error) {
                return res.serverError(error);
            });
        } else {
            return res.serverError({ message: 'no userId' });
        }
    },

    updatePaymentSource: function(req, res){
        var userId = req.body.userId;
        var cardInfo = req.body.cardInfo;
        var index = req.body.index;
        if (userId) {
            TransactionService.updatePaymentSource(userId, cardInfo, index).then(function (resp) {
                return res.ok(resp);
            }).catch(function(error) {
                return res.serverError(error);
            });
        } else {
            return res.serverError({ message: 'no userId' });
        }
    },

    deletePaymentSource: function(req, res){
        var userId = req.body.userId;
        var index = req.body.index;
        if (userId) {
            TransactionService.deletePaymentSource(userId, index).then(function (resp) {
                return res.ok(resp);
            }).catch(function(error) {
                return res.serverError(error);
            });
        } else {
            return res.serverError({ message: 'no userId' });
        }
    },

    createPaymentSource: function(req, res){
        var userId = req.body.userId;
        var token = req.body.token;

        if (userId && token) {
            TransactionService.createPaymentSource(userId, token).then(function (customer) {
                return res.ok(customer);
            }).catch(function(error) {
                return res.serverError(error);
            });
        } else {
            return res.serverError({ message: 'no userId' });
        }
    },

    createCustomer: function(req, res) {
        var userId = req.body.userId;
        var token = req.body.token;
         console.log(req.body);
        if (userId && token) {
            TransactionService.createCustomer(userId, token).then(function (customer) {
                return res.ok(customer);
            }).catch(function(error) {
                return res.serverError(error);
            });
        } else {
            return res.serverError({ message: 'no userId' });
        }
    },

    createOrder: function(req, res) {
        var customerId = req.body.customerId;
        var userId = req.body.userId;
        var medicId = req.body.medicId;
        var paymentSource = req.paymentSource;
        TransactionService.createTransaction(customerId, userId, medicId, paymentSource).then(function(transaction){
            console.log(transaction);
            res.ok(transaction)
        }).catch(function(err){
            return res.serverError(err);
        });
    },

    createTransaction: function(req, res) {
        var userId = req.body.userId;
        var medicId = req.body.medicId;
        var paymentId = req.body.paymentId;
        console.log(paymentId);
        TransactionService.createTransactionPaypal(userId, medicId, paymentId).then(function(transaction){
            console.log(transaction);
            res.ok(transaction)
        }).catch(function(err){
            return res.serverError(err);
        });
    },

	addCard: function(req, res) {
        var customer = TransactionService.createCustomer(req.body, parseInt(req.body.patient));
        customer.then(function(c) {
            TransactionService.createTransaction(
                c,
                parseInt(req.body.patient),
                parseInt(req.body.medic),
                parseInt(req.body.caseId)
            ).then(function(response){
                res.redirect('/transactions-list');
            }).catch(function(err){
                console.error(err);
                res.serverError(err);
            })
        });
    }
};

