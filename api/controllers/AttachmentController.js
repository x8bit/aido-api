/**
 * AttachmentController
 *
 * @description :: Server-side logic for managing attachments
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
const fs = require('fs');
const path = require('path');

module.exports = {

  form: function (req, res) {
    res.writeHead(200, {'content-type': 'text/html'});
    return res.end(
      '<form action="/attachment/upload" enctype="multipart/form-data" method="post">'+
      '<input type="text" name="message" placeholder="message"><br>'+
      '<input type="text" name="case" placeholder="case"><br>'+
      '<input type="file" name="file"><br>'+
      '<input type="submit" value="Upload">'+
      '</form>'
    );
  },

  findOne: function (req, res) {
    Attachment.findOne().where({ id: req.params.id }).exec( function (err, file) {
      if (err) { return res.serverError(err); }

      if (err || !file) {
        return res.status(404).end();
      } else {
        let isThumb = req.query.thumb;
        if (isThumb) {
          let path = file.thumbnail || file.path;
          return res.status(200).sendfile(path);
        } else {
          return res.status(200).sendfile(file.path);
        }
      }
    });
  },

  upload: function (req, res) {
    const caseId = req.query.case;
    const messageId = req.query.message;

    if (caseId && messageId) {
      req.file('file').upload({ dirname: path.join(process.cwd(), '..', 'attachments') }, function (err, files) {
        if (err) { return res.serverError(err); }

        const file = files[0];

        let newAttachment = {
          filename: file.filename,
          size: file.size,
          type: file.type,
          ext: file.filename.split('.').pop(),
          path: file.fd,
          case: caseId,
          message: messageId
        };

        Attachment.create(newAttachment).exec(function (err, att) {
          if (err) { return res.serverError(err); }

          if (ImageService.isImage(att.type)) {
            ImageService.resizeImage(file.fd, att.id, file.filename, 200, 200).then((r) => {

              Attachment.update({ id: att.id}, { thumbnail: r }).exec((err, image) => {
                if (err) { return res.serverError(err); }
                return res.ok({ id: image[0].id });
              });

            });
          } else {
            return res.ok({ id: att.id });
          }
        });

      });
    } else {
      return res.serverError({ message: "must provide case or message" });
    }
  }

};

