/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
const bcrypt = require('bcryptjs');
const crypto = require('crypto');

module.exports = {

  forgot: function(req, res) {
    var email = req.body && req.body.email;
    if (email) {

      User.findOneByEmail(email).exec(function (err, user) {
        if (err) { return res.serverError(err); }

        if (user) {
          crypto.randomBytes(16, function(err, buf) {
            if (err) { return res.serverError(err); }
            var token = buf.toString('hex');

            User.update({ id: user.id }, { recoveryToken: token }).exec(function (err, updatedUser) {
              if (err) { return res.serverError(err); }
              var link = 'http://52.11.251.122/api/recoverPassword?email=' + updatedUser[0].email + '&token=' + token;
              var url = 'Recuperar contraseña ' + link;
              SendGridService.sendSimpleMail('Recuperar contraseña', 'admin@aido.mx', updatedUser[0].email, url);
              return res.ok("Enviado");
            });

          });

        } else {
          return res.serverError({ message: 'user not found' });
        }
      });

    } else {
      return res.serverError({ message: 'email is required' });
    }
  },

  recovery: function (req, res) {
    var body = req.body || {};
    var email = body.email;
    var token = body.token;
    var password = body.password;

    console.log(email, token, password)

    if (!email || !token || !password) {
      return res.serverError({ message: 'fields missing' });
    } else {
      User.findOne({ email: email, recoveryToken : token}).exec(function(err, userFound) {
        if (err) { return res.serverError(err); }
        if (userFound) {
          User.update({ email: email }, { recoveryToken: null, password: password }).exec(function (err, userUpdated) {
            if (err) { return res.serverError(err); }

            console.log(userUpdated);

            return res.ok("Contraseña cambiada");
          });
        } else {
          return res.serverError({ message: 'user not found' });
        }
      });
    }
  },

  showRecover: function (req, res) {
    res.view('recover', {
      layout: null
    });
  },

  list: function (req, res) {
    var query = { sort: { name: 1, lastname: 1 } };
    User.find(query).populate(['role', 'specialties', 'profile']).then(function(users) {
      const medics = users.filter((u) => { return u.role[0].id == 3 });
      res.view('users', {
        users: medics
      });
    }).catch(function(err) {
      return res.serverError(err);
    });

  },

  findOne: function (req, res) {
    const userId = req.param('id');

    if (userId) {
      User.findOne({ where: { id: userId } }).populate(['role', 'specialties', 'profile']).exec(function (err, _userFound) {
        if (err) { return res.serverError(err); }
        var userFound = _userFound;
        Config.find().exec(function(err, c) {
          if (err) { return res.serverError(err); }
          if (!userFound.rate) {
            userFound.rate = c[0].minRate || 0;
          }
          return res.ok(userFound);
        });
      });
    } else {
      return res.serverError({ message: 'user not found' });
    }
  },

  findPatient: function (req, res) {
    const search = req.query.email;
    let query = {};

    if (search) {
      query = { or: [
        { email: search, active: true },
      ], sort: { name:  1, lastname: 1 } };
    }

    User.findOne(query).populate(['role', 'profile', 'specialties']).then(function(user) {
      if (!user) {
        SearchLogService.register(search, false);
      }

      if (user && user.role[0].id == 2) {
        return res.ok([user]);
      } else {
        return res.ok([]);
      }

    }).catch(function(err) {
      return res.serverError(err);
    });
  },

  deleteToken: function (req, res) {
    const token = req.body.device_token;
    const user  = req.body.user;
    const platform  = req.body.platform;
    console.log('DELETE TOKEN ---------');
    console.log(token, user, platform);
    if (token) {
      Device.destroy({ where: { token: token } })
        .then(function (deletedToken) {
          console.log(deletedToken);
          return User.findOne({ where: { id: user } });
        })
        .then(function(userFound) {
          return res.ok(userFound);
        })
        .catch(function (error) {
          return res.serverError(error);
        });
    } else {
      return res.serverError({ error: 'no token' });
    }
  },

  receiveToken: function(req, res) {
    const token = req.body.device_token;
    const user  = req.body.user;
    const platform  = req.body.platform;
    console.log('ADD TOKEN ---------');
    console.log(token, user, platform);
    if (token) {
      var _newDevice = {};
      Device.create({ platform: platform, token: token })
        .then(function (newDevice) {
          _newDevice = newDevice;
          return User.findOne({ where: { id: user } });
        })
        .then(function (userFound) {
          userFound.devices.add(_newDevice.id);
          userFound.save(function () {
            res.ok(userFound);
          })
        })
        .catch(function (error) {
          return res.serverError(err);
        });
    } else {
      return res.serverError({ error: 'no token' });
    }
  },

  inviteUser: function(req, res) {
    const email = req.body.email;
    const fromEmail = req.body.fromEmail;
    if (email && fromEmail) {

      User.findOneByEmail(fromEmail).populate(['role', 'profile', 'specialties']).exec(function (err, user) {
        if (err) { return res.serverError(err); }
        let msg = "Dr. " + user.name + " " + user.lastname + " lo invita a descargar Aido para dar seguimiento a su consulta."
        SendGridService.sendSimpleMail('Invitación Consulta Médica', 'admin@aido.mx', email, msg);
        return res.ok("Enviado");
      });

    } else {
      return res.serverError({ message: "no email" });
    }
  },

  inviteNewUser: function(req, res) {
    const email = req.body.email;
    const fromEmail = req.body.fromEmail;
    console.log(email, fromEmail);
    if (email && fromEmail) {

      User.findOneByEmail(fromEmail).populate(['role', 'profile', 'specialties']).exec(function (err, user) {
        if (err) { return res.serverError(err); }
        let msg = "Dr. " + user.name + " " + user.lastname + " lo invita a descargar Aido para dar seguimiento a su consulta."

        SendGridService.sendSimpleMail('Invitación Consulta Médica', 'admin@aido.mx', email, msg);

        return res.ok("Enviado")
      });

    } else {
      return res.serverError({ message: "no email" });
    }
  },

  refreshToken: function(req, res) {
    const user  = req.body.user;
    if (user) {
      User.findOne({ id: user }).populate(['role', 'specialties']).exec(function(err, user) {
        if (err) { return res.serverError(err); }
        return res.ok({ user: user, token: jwToken.issue({ id : user.id }) });
      })
    }
  },

  sendReport: function(req, res) {
    const email = req.body.email;
    const message = req.body.message;
    const subject = req.body.subject;
    if (email && message && subject) {
      SendGridService.sendSimpleMail('Reportar a Aido - ' + subject, email, 'admin@aido.mx', message);
      return res.ok("Enviado")
    } else {
      return res.serverError({ message: "missing params" });
    }
  },


  create: function (req, res) {
    const email = req.body.email;

    if (email) {
      User.findOneByEmail(email).exec(function(err, user) {
        if (err) { return res.serverError(err); }

        if (!user) {
          User.create(req.body).exec(function (err, newUser) {
            if (err) { return res.serverError(err); }

            if (!newUser) {
              return res.serverError({ message: 'Ocurrio un error creando la cuenta' });
            } else {
              SendGridService.sendSimpleMail('Bienvenido', 'admin@aido.mx', newUser.email, 'Gracias por crear tu cuenta!');
              return res.created({ user: newUser, token: jwToken.issue({id: newUser.id}) });
            }
          });
        } else {
          return res.serverError({ message: 'El correo electrónico ya esta registrado en la aplicación' });
        }
      });
    } else {
      return res.notFound({ message: 'Ocurrio un error creando la cuenta' });
    }

  },

  findMedics: function (req, res) {
    const search = req.query.search;
    let query = {};

    if (search) {
      query = { or: [
        { name: { contains: search.toLowerCase() }, active: true },
        { lastname: { contains: search.toLowerCase() }, active: true }
      ], sort: { name:  1, lastname: 1 } };
    } else {
      query = { where: { active: true }, sort: { name: 1, lastname: 1 } };
    }

    User.find(query).populate(['role', 'profile', 'specialties']).then(function(users) {
      var medics = users.filter((u) => { return u.role[0].id == 3 });
      if (search && medics.length !== 0) SearchLogService.register(search, (medics.length !== 0));
      Config.find().then(function(c){
        console.log(c);
        return res.ok(medics.map(function(m){
          if (typeof m.rate == 'undefinded' || !m.rate) {
            console.log(m);
            m.rate = c[0].minRate || 0;
          }
          return m;
        }));
      }).catch(function(err) {
        return res.serverError(err);
      });
    }).catch(function(err) {
      return res.serverError(err);
    });
  },

  sess: function (req, res) {
    if (!req.session.user) {
      return res.forbidden({ message: "No active session please login" })
    }
    return res.ok(req.session.user);
  },

  logout: function (req, res) {
    req.session.user = null;
    return res.ok({ message: 'Session terminated' });
  },

  login: function (req, res) {
    const email = req.body.email;
    const pass = req.body.password;

    if (!email || !pass) {
      return res.notFound({ message: 'Email and password are required' });
    } else {
      User.findOneByEmail(email).populate(['role', 'profile', 'specialties']).exec(function (err, user) {
        if (err) { return res.serverError(err); }

        if (user) {
          if (!user.active) { return res.serverError({ message: 'El correo electrónico no esta activo' }); }

          User.comparePassword(req.body.password, user.hashedPassword, function (err, match) {
            if (err) { return res.forbidden(err); }

            if (!match) {
              return res.serverError({ message: 'La combinación de correo electrónico y contraseña no es correcta' });
            } else {
              return res.ok({ user: user, token: jwToken.issue({ id : user.id }) });
            }
          });

        } else {
          return res.notFound({ message: 'La combinación de correo electrónico y contraseña no es correcta' });
        }
      });
    }
  },

  loginWeb: function(req, res){
    const email = req.body.email;
    const pass = req.body.password;

    if (!email || !pass) {
      return res.notFound({ message: 'Email and password are required' });
    } else {
      User.findOneByEmail(email).populate(['role', 'specialties']).exec(function (err, user) {
        if (err) { return res.serverError(err); }

        if (user) {
          if (!user.active) { return res.serverError({ message: 'User is not active' }); }

          User.comparePassword(req.body.password, user.hashedPassword, function (err, match) {
            if (err) { return res.forbidden(err); }

            if (!match) {
              return res.serverError({ message: 'The user and password combination is not right' });
            } else {
              if (user.role[0].id == 1 ) {
                req.session.user = user;
                delete req.session.user.hashedPassword;
                return res.redirect('/api/user-list');
                //return res.ok({ user: user, token: jwToken.issue({ id : user.id }) , admin:true});
              }
                return res.serverError({ message: 'User is not admin' });
            }
          });

        } else {
          return res.notFound({ message: 'User does not exists' });
        }
      });
    }
  }

};
